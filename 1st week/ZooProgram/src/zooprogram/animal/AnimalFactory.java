
package zooprogram.animal;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import jdk.nashorn.internal.objects.Global;


public class  AnimalFactory implements Constants {
    
    private static Random r = new Random();
      
    private static Date createDate(int year, int month, int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        return c.getTime();
    }   
    
    private static AnimalColor getRandomColor(){
        int rand = r.nextInt(3);
        switch (rand) {
            case 0:
                return AnimalColor.BLACK;
            case 1:
                return AnimalColor.WHITE;
            default:
                return AnimalColor.ELSE;
        }
    }
    
    public static Animal getAnimal(String name){
        Animal animal = null;
        switch(name){
            case CAT:
                return new Cat(getRandomColor(), createDate(r.nextInt(48) + 1970, r.nextInt(12) + 1, r.nextInt(31) + 1));
            case BAT:
                return new Bat(getRandomColor(), createDate(r.nextInt(48) + 1970, r.nextInt(12) + 1, r.nextInt(31) + 1));
            case CHICKEN:
                return new Chicken(getRandomColor(), createDate(r.nextInt(48) + 1970, r.nextInt(12) + 1, r.nextInt(31) + 1));
            case AMUR:
                return new Amur(getRandomColor(), createDate(r.nextInt(48) + 1970, r.nextInt(12) + 1, r.nextInt(31) + 1));
            case RAVEN: 
                return new Raven(getRandomColor(), createDate(r.nextInt(48) + 1970, r.nextInt(12) + 1, r.nextInt(31) + 1));
        }
        
        return animal;
    }
    
    
    
    
}
