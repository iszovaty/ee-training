/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zooprogram.animal;

import java.util.Date;

/**
 *
 * @author szotyi
 */
public class Raven extends Bird implements Fly {
    
    public Raven(AnimalColor color, Date birthDate) {
        super(color, birthDate);
    }


    @Override
    public void testFly() {
        System.out.println("IM A FLYING RAVEN");
    }
    
    
   
    
}
