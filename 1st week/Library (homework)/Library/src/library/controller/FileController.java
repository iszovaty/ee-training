
package library.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.exception.InvalidFileInputException;
import library.exception.InvalidInputException;
import library.model.Author;
import library.model.AuthorFactory;
import library.model.Book;
import library.model.BookCategory;
import library.model.BookFactory;
import library.model.Constants;
import library.repository.FileRepository;


public class FileController implements FileRepository, Constants {

    private LibraryController libraryController;
    
    public FileController(LibraryController libraryController){
        this.libraryController = libraryController;
    }
    
    @Override
    public void fileLoad(String fileName) throws FileNotFoundException, InvalidFileInputException {
        Scanner scanner = new Scanner(new File(fileName));
        while(scanner.hasNext()){
            String[] line = scanner.nextLine().split(";");
            Integer id = Integer.parseInt(line[0]);
            Author author = null;
            if(!line[1].isEmpty()){
                Integer authorId = Integer.parseInt(line[1]);
                String authorName = line[2];
                author = libraryController.getAuthorController().findById(authorId);
                if(author == null){
                    author = AuthorFactory.getAuthor(authorName);
                    libraryController.getAuthorController().getRegistedAuthors().add(author);
                }
            }
            String title = line[3];
            String isbn = line[4];
            Integer numbOfPages = Integer.parseInt(line[5]);
            Integer categoryNumber = Integer.parseInt(line[6]);
            BookCategory category = BookCategory.get(categoryNumber);
            Book book = BookFactory.createBook(author, title, isbn, numbOfPages ,category);
            if(!title.isEmpty() && !isbn.isEmpty() && numbOfPages > 0 && category != null){
                if(fileName.equals("rented.txt")){
                    libraryController.getBookController().saveTaken(book);
                } else if(fileName.equals("storaged.txt")){
                    libraryController.getBookController().save(book);
                }
            } else {
                throw new InvalidFileInputException("Invalid file inputs!");   
            }
        }
        scanner.close();
    }

    
    private Integer getCategoryNumber(Book book){
        switch(book.getClass().getSimpleName()){
            case "ScifiBook": 
                return 0;
            case "StoryBook":
                return 1;
            case "CookBook":
                return 2;
            case "EducationalBook":
                return 3;
            case "LiteratureBook":
                return 4;
        }
        return null;
    }
    
    @Override
    public void updateStoraged() throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter("storaged.txt");
        for(Book item : libraryController.getAllBooks()){
            String authorName = (item.getAuthor() != null) ? item.getAuthor().getName() : "";
            Author author = libraryController.getAuthorController().findByName(authorName);
            String authorId = (author != null) ? author.getId().toString() : "";
            printWriter.println(item.getId() + ";"  + authorId + ";" + authorName + ";" + item.getTitle() + ";" + item.getIsbn() + ";" + item.getNumbOfPages() + ";" + getCategoryNumber(item));
        }
        printWriter.close();
    }

    private void updateFileContent(List<Book> books, PrintWriter pw){
        for(Book item : libraryController.getAllBooks()){
            String authorName = (item.getAuthor() != null) ? item.getAuthor().getName() : "";
            Author author = libraryController.getAuthorController().findByName(authorName);
            String authorId = (author != null) ? author.getId().toString() : "";
            pw.println(item.getId() + ";"  + authorId + ";" + authorName + ";" + item.getTitle() + ";" + item.getIsbn() + ";" + item.getNumbOfPages() + ";" + getCategoryNumber(item));
        }
    }
    
    public void updateFile(String fileName) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(fileName);
        if(fileName.equals("storaged.txt")){
            updateFileContent(BookController.getBooksOnStorage(), printWriter);
        } else if(fileName.equals("rented.txt")){
            updateFileContent(BookController.getBooksTaken(), printWriter);
        }
        printWriter.close();
    }
    
    @Override
    public void updateRented() throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter("rented.txt");
        for (Book item : libraryController.getTakenBooks()) {
            String authorName = (item.getAuthor() != null) ? item.getAuthor().getName() : "";
            Author author = libraryController.getAuthorController().findByName(authorName);
            String authorId = (author != null) ? author.getId().toString() : "";
            printWriter.println(item.getId() + ";" + authorName + ";" + item.getTitle() + ";" + item.getIsbn() + ";" + getCategoryNumber(item));
        }
        printWriter.close();
    }
    
}
