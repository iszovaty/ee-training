
package library.controller;

import java.util.ArrayList;
import java.util.List;
import library.model.Author;
import library.repository.AuthorRepository;


public class AuthorController implements AuthorRepository {

    private static List<Author> registedAuthors = new ArrayList<>();

    public static List<Author> getRegistedAuthors() {
        return registedAuthors;
    }
    
    
    
    
    @Override
    public List<Author> findAll() {
        List<Author> authors = new ArrayList<>();
        registedAuthors.forEach(item -> authors.add(item));
        return authors;
    }

    @Override
    public Author findById(Integer id) {
        for(Author item : registedAuthors){
            if(item.getId() == id){
                return item;
            }
        }
        return null;
    }

    public Author findByName(String name){
        Author author = null;
        for (Author item : registedAuthors) {
            if(item.getName().equals(name)){
                return item;
            }
        }
        return author;
    }
    
    @Override
    public void save(Author author) {
        if(author != null){
            registedAuthors.add(author);
        }
    }

    @Override
    public void remove(Author author) {
        if(author != null){
            registedAuthors.remove(author);
        }
    }
    
    
}
