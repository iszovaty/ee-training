/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.repository;

import java.util.List;

/**
 *
 * @author doobs
 */
public interface BaseRepository<T> {
    
    public List<T> findAll();
    public T findById(Integer id);
    
}
