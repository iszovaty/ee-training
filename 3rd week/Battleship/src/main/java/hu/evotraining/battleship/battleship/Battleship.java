
package hu.evotraining.battleship.battleship;

import hu.evotraining.battleship.battleship.logic.Game;
import java.io.FileNotFoundException;


public class Battleship {

    private Game game;
    
    public Battleship(Game game) throws FileNotFoundException {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    
    
    
    
    
    
    
}
