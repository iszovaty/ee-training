
package library.model;

import java.util.List;
import library.repository.AuthorRepository;

public class AuthorFactory {
    
    private static Integer generatedId = 1;
    
    public static Author getAuthor(String name){
        Author author = null;
        if(!name.isEmpty()){
            author = Author.getInstance();
            author.setId(generatedId++);
            author.setName(name);
        }
        return author;
    }
    
}
