
package library.repository;

import java.util.ArrayList;
import java.util.List;
import library.model.Author;


public interface AuthorRepository extends BaseRepository<Author> {
    
    public void save(Author author);
    public void remove(Author author);
    
}
