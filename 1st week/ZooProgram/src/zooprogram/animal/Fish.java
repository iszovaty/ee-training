/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zooprogram.animal;

import java.util.Date;

/**
 *
 * @author szotyi
 */
public abstract class Fish extends Animal {
    
    protected int length;
    
    public Fish(AnimalColor color, Date birthDate) {
        super(color, birthDate);
    }

    public Fish(AnimalColor color, Date birthDate, int length) {
        super(color, birthDate);
        this.length = length;
    }

    
    
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
    
    
    
}
