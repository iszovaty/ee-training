/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.model;

/**
 *
 * @author doobs
 */
public interface Constants {
    
    public static final String SCIFI = "SCIFI";
    public static final String COOKBOOK = "COOKBOOK";
    public static final String LITERATURE = "LITERATURE";
    public static final String STORYBOOK = "STORYBOOK";
    public static final String EDUCATIONAL = "EDUCATIONAL";
    
}
