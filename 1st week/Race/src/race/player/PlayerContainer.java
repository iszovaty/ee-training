
package race.player;

import java.util.ArrayList;
import java.util.List;
import race.exceptions.DrawException;


public class PlayerContainer {
    
    private List<Player> players = new ArrayList<Player>();

    public PlayerContainer() {
    }

    
    
    
    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
       
    public Player getBestPlayer() throws DrawException{
        Player bestPlayer = null;
        for (int i = 0; i < players.size(); i++) {
            //next time I'll cal it hasValidJump instead.
            if(bestPlayer == null){
                if(players.get(i).getValidJump()){
                    bestPlayer = players.get(i);
                }
            } else {
                if(players.get(i).bestResult()[0] > bestPlayer.bestResult()[0]){
                    bestPlayer = players.get(i);
                } else if(players.get(i).bestResult()[0] == bestPlayer.bestResult()[0]){
                    if(players.get(i).bestResult()[1] < bestPlayer.bestResult()[1]){
                        bestPlayer = players.get(i);
                    } else if(players.get(i).bestResult()[1] == bestPlayer.bestResult()[1]){
                        throw new DrawException("No winner exists!");
                    } else {
                        //...
                    }
                } else {
                    
                }
            }
        }
        return bestPlayer;
    }
    
    
    
    
}
