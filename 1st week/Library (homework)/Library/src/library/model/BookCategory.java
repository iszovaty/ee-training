/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.model;

/**
 *
 * @author doobs
 */
public enum BookCategory {
 
    SCIFI(0),
    STORYBOOK(1),
    COOKBOOK(2),
    EDUCATIONAL(3),
    LITERATURE(4);
    
    private int type;

    
    BookCategory(int type){
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    
    public static BookCategory get(int type){
        switch(type){
            case 0:
                return BookCategory.SCIFI;
            case 1:
                return BookCategory.STORYBOOK;
            case 2:
                return BookCategory.COOKBOOK;
            case 3:
                return BookCategory.EDUCATIONAL;
            case 4:
                return BookCategory.LITERATURE;
        }
        return null;
    }
    
}
