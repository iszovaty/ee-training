
package library.view;

import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Scanner;
import library.controller.BookController;
import library.controller.FileController;
import library.controller.LibraryController;
import library.exception.InvalidBookAssignmentException;
import library.exception.InvalidFileInputException;
import library.exception.InvalidInputException;
import library.model.Author;
import library.model.AuthorFactory;
import library.model.Book;
import library.model.BookCategory;
import library.model.BookFactory;


public class ControlGUI {
 
    private static LibraryController libraryController;
    private static FileController fileController;
      
    public ControlGUI(){
        libraryController = new LibraryController();
        fileController = new FileController(libraryController);
        initFromFile();
                
    }
    
    private void initFromFile(){
        String storageFile = "storaged.txt";
        String rentFile = "rented.txt";
        try {
            fileController.fileLoad(storageFile);
            fileController.fileLoad(rentFile);
        } catch(FileNotFoundException fnfe){
            System.out.println(fnfe.getMessage());
        } catch(InvalidFileInputException ifie){
            System.out.println(ifie.getMessage());
        } 
    }
    
    private void listBooks(){
        Collections.sort(libraryController.getAllBooks());
        libraryController.getAllBooks().stream().forEach(item -> System.out.println(item.toString()));
    }
    
    
    private void showMenu(){
        System.out.println("-----------------MENU-----------------");
        System.out.println("(Choose one of the options and press enter)");
        System.out.println("1) Show books on storage");
        System.out.println("2) Add new book");
        System.out.println("3) Add new writer");
        System.out.println("4) Assign book to writer");
        System.out.println("5) Rent a book");
        System.out.println("6) Book return");
        System.out.println("7) Show registered authors");
        System.out.println("8) Quit");
        System.out.println("--------------------------------------");
    }
    
    private void addNewBook() throws InvalidInputException{
        Scanner bookAddScanner = new Scanner(System.in);
        System.out.println("Book title? ");
        String title = bookAddScanner.nextLine();
        if(title.isEmpty()){
            throw new InvalidInputException("Failed! Title is empty");
        } else {
            System.out.println("ISBN number?");
            String isbn = bookAddScanner.nextLine();
            if(isbn.isEmpty() || (isbn.length() != 13 && isbn.length() != 10) ){
                throw new InvalidInputException("Failed! ISBN must be 10 or 13 characters long");
            } else {
                System.out.println("Number of pages? ( ? > 0)!!!"); 
                Integer numbOfPages = Integer.parseInt(bookAddScanner.nextLine());
                if(numbOfPages < 2){
                    throw new InvalidInputException("Number of pages must be 2+ ");
                } else {
                    System.out.println("Choose the category (type that number which belongs to the movie type, then press enter)");
                    System.out.println("0) SCIFI 1) STORYBOOK 2) COOKBOOK 3) EDUCATIONAL 4) LITERATURE");
                    Integer categoryId = bookAddScanner.nextInt();
                    BookCategory category = BookCategory.get(categoryId);
                    System.out.println(category);
                    if(category == null){
                        throw new InvalidInputException("Category value must be in [0-4]!");
                    } else {
                        Book book = BookFactory.createBook(null, title, isbn, numbOfPages, category);
                        if(book == null){
                            //maybe this conditional check is unnecessary
                            throw new InvalidInputException("Book creation failed!");
                        }
                        libraryController.insertBook(book);
                    }
                }
            }
        }
    }
    
    private void addBook(){
        try {
            addNewBook();
            fileController.updateFile("storaged.txt");
        } catch(InvalidInputException iie){
            System.out.println(iie.getMessage());
        } catch(FileNotFoundException fnfe){
            System.out.println(fnfe.getMessage());
        } catch(NumberFormatException nfe){
            System.out.println(nfe.getMessage());
        }
    }
    
    private void addNewAuthor() throws InvalidInputException{
        System.out.println("Author's name? ");
        Scanner authorAddScanner = new Scanner(System.in);
        String name = authorAddScanner.nextLine();
        if(name.isEmpty()){
            throw new InvalidInputException("Name cannot be empty!");
        }
        Author author = AuthorFactory.getAuthor(name);
        if(author == null){
            throw new InvalidInputException("Author creation failed!");
        }
        libraryController.insertAuthor(author);
    }
    
    private void addAuthor(){
        try {
            addNewAuthor();
        } catch(InvalidInputException iie){
            System.out.println(iie.getMessage());
        } 
    }
    
    private void assignBookToAuthor(){
        Scanner assignScanner = new Scanner(System.in);
        System.out.println("Enter the author's ID!");
        System.out.println("If you are not sure in the ID, check regisreted authors first by using the show registered authors function (To do this, type -1 here and then type 7 and press enter in the main menu)");

        try {
            Integer authorId = Integer.parseInt(assignScanner.nextLine());
            if(authorId == -1){
                return;
            } 
            System.out.println("Enter the book you want to assign to the author!");
            String title = assignScanner.nextLine();
            libraryController.assignBookTo(title, authorId);
            fileController.updateFile("storaged.txt");
        }catch(InvalidInputException | InvalidBookAssignmentException | FileNotFoundException | NumberFormatException iie){
            System.out.println(iie.getMessage());
        }
    }
    
    private void rentBook(){
        Scanner rentScanner = new Scanner(System.in);
        System.out.println("Author?");
        String authorName = rentScanner.nextLine();
        System.out.println("Title?");
        String title = rentScanner.nextLine();
        try {
            libraryController.rentBook(authorName, title);
            fileController.updateFile("storaged.txt");
            fileController.updateFile("rented.txt");
        } catch(InvalidInputException iie){
            System.out.println(iie.getMessage());
        } catch(FileNotFoundException fnfe){
            System.out.println(fnfe.getMessage());
        }
    }
    
    private void retakeBook(){
        Scanner retakeScanner = new Scanner(System.in);
        System.out.println("Author?");
        String authorName = retakeScanner.nextLine();
        System.out.println("Title?");
        String title = retakeScanner.nextLine();
         try {
            libraryController.retakeBook(authorName, title);
            fileController.updateFile("storaged.txt");
            fileController.updateFile("rented.txt");
        } catch(InvalidInputException iie){
            System.out.println(iie.getMessage());
        } catch(FileNotFoundException fnfe){
            System.out.println(fnfe.getMessage());
        }
    }

    
    private void listRegisteredAuthors(){
        libraryController.getAllAuthors().stream().forEach(item -> System.out.println(item.toString()));
    }
    
    public void menuControl(){
        initFromFile();
        Scanner menuControlScanner = new Scanner(System.in);
        showMenu();
        Integer option = 0;    
        while(option != 8){
            option = menuControlScanner.nextInt();
            switch(option){
                case 1:                
//                    libraryController.getAllBooks();.stream().forEach(item -> System.out.println(item.toString()));
                    listBooks();
                    showMenu();
                    break;
                case 2:
                    addBook();
                    showMenu();
                    break;
                case 3:
                    addAuthor();
                    showMenu();
                    break;
                case 4:
                    assignBookToAuthor();
                    showMenu();
                    break;
                case 5:
                    rentBook();
                    showMenu();
                    break;
                case 6:
                    retakeBook();
                    showMenu();
                    break;
                case 7:
                    listRegisteredAuthors();
                    showMenu();
                    break;
                case 8:
                    System.exit(0);
                    break;
            }
        }
    }
    
    
}
