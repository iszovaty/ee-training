
package library.controller;

import java.util.ArrayList;
import java.util.List;
import library.model.Author;
import library.model.Book;
import library.repository.BookRepository;


public class BookController implements BookRepository {
    
    private static List<Book> booksOnStorage = new ArrayList<>();
    private static List<Book> booksTaken = new ArrayList<>();

    
    
    public static List<Book> getBooksOnStorage() {
        return booksOnStorage;
    }

    public static List<Book> getBooksTaken() {
        return booksTaken;
    }
    
    @Override
    public List<Book> findAll() {
        List<Book> books = new ArrayList<>();
        booksOnStorage.forEach(item -> books.add(item));
        return books;
    }

    @Override
    public Book findById(Integer id) {
        for(Book item : booksOnStorage){
            if(item.getId().equals(id)){
                return item;
            }
        }
        return null;
    }
    
    @Override
    public List<Book> findAllTakens(){
        List<Book> books = new ArrayList<>();
        booksTaken.forEach(item -> books.add(item));
        return books;
    }
    
    @Override
    public List<Book> findByAuthor(Author author) {
        List<Book> filteredBooks = new ArrayList<>();
        booksOnStorage.stream().filter(item->item.getAuthor().getName().equals(author.getName())).forEach(item -> filteredBooks.add(item));
        return filteredBooks;
    }

    @Override
    public Book findByAuthorAndTitle(Author author, String title){
        Book book = null;
        for(Book item : booksOnStorage) {
            if(item.getAuthor() != null){
                if(item.getAuthor().getName().equals(author.getName()) && item.getTitle().equals(title)){
                    return item;
                }
            } 
        }
        return book;
    }

    @Override
    public Book findRentedByAuthorAndTitle(Author author, String title){
        Book book = null;
        for(Book item : booksTaken){
            if(item.getAuthor().getName().equals(author.getName()) && item.getTitle().equals(title)){
                return item;
            }
        }
        return book;
    }
    
    @Override
    public Book findByTitleWithoutAuthor(String title){
        Book book = null;
        for(Book item : booksOnStorage){
            if(item.getTitle().equals(title) && item.getAuthor() == null){
                return item;
            }
        }
        return book;
    }
    
    @Override
    public void rentBook(Book book){ 
        booksTaken.add(book);
        remove(book);
    }
    
    @Override
    public void retakeBook(Book book){
        save(book);
        booksTaken.remove(book);
    }
    
    @Override
    public void save(Book book) {
        if(book != null ){
            booksOnStorage.add(book);
        } 
    }
    
    public void saveTaken(Book book){
        if(book != null){
            booksTaken.add(book);
        }
    }

    @Override
    public void remove(Book book) {
        booksOnStorage.remove(book);
    }


    
}
