
package library.controller;

import java.util.List;
import library.exception.InvalidBookAssignmentException;
import library.exception.InvalidInputException;
import library.model.Author;
import library.model.Book;

//Controller Manager-ként funkcionál
public class LibraryController  {

    private BookController bookController = new BookController();
    private AuthorController authorController = new AuthorController();
    
    public LibraryController(){
        
    }
    
    public BookController getBookController() {
        return bookController;
    }

    public void setBookController(BookController bookController) {
        this.bookController = bookController;
    }

    public AuthorController getAuthorController() {
        return authorController;
    }

    public void setAuthorController(AuthorController authorController) {
        this.authorController = authorController;
    }
    
    public List<Book> getAllBooks(){
        return bookController.findAll();
    }
    
    public List<Author> getAllAuthors(){
        return authorController.findAll();
    }
    
    public List<Book> getTakenBooks(){
        return bookController.getBooksTaken();
    }
    
    public List<Book> getBooksByAuthor(Author author){
        return bookController.findByAuthor(author);
    }
    
    public void insertBook(Book book){
        bookController.save(book);
    }
    
    public void insertAuthor(Author author){
        authorController.save(author);
    }
    
    public void removeBook(Book book){
        bookController.remove(book);
    }
    
    public void assignBookTo(String title, Integer id) throws InvalidBookAssignmentException, InvalidInputException{
        if(!title.isEmpty() && id >= 0){          
            Author author = authorController.findById(id);
            if(author != null){
                Book book = bookController.findByTitleWithoutAuthor(title);
                if(book != null){
                    if(!author.getWrittenBooks().contains(book)){
                        author.getWrittenBooks().add(book);
                        book.setAuthor(author);
                    } else {
                        throw new InvalidBookAssignmentException("Book assignment failed! Author already has a book, with the given title");
                    }
                } else {
                    throw new InvalidBookAssignmentException("No book exists with the given title");
                }  
            } else {
                throw new InvalidBookAssignmentException("No author registered with the given name!");
            }      
        } else {
            throw new InvalidInputException("Book assignment failed!");
        } 
    }  
    
    public void rentBook(String authorName, String title) throws InvalidInputException{
        if(!authorName.isEmpty() && !title.isEmpty()){
            Author author = authorController.findByName(authorName);
            if(author != null){
                Book book = bookController.findByAuthorAndTitle(author, title);
                if(book != null){
                    bookController.rentBook(book);
                } else {
                    throw new InvalidInputException("Book does not exists!");
                } 
            } else {
                throw new InvalidInputException("Author does not exist!");
            }
        } else{
            throw new InvalidInputException("Authorname and title cannot be empty!");
        }
    }    
    
    public void retakeBook(String authorName, String title) throws InvalidInputException{
        if(!authorName.isEmpty() && !title.isEmpty()){
            Author author = authorController.findByName(authorName);
             if(author != null){
                Book book = bookController.findRentedByAuthorAndTitle(author, title);
                if(book != null){
                   bookController.retakeBook(book);
                } else {
                    throw new InvalidInputException("Book does not exists!");
                } 
            } else {
                throw new InvalidInputException("Author does not exists!");
            }
        } else {
            throw new InvalidInputException("Authorname and title cannot be empty!");
        }
    }  
    
    
}


