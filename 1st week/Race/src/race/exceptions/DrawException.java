/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package race.exceptions;

/**
 *
 * @author doobs
 */
public class DrawException extends Exception {
    
    
    private String message;

    public DrawException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return message;
    }
    
}
