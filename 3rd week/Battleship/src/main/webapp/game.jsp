<%-- 
    Document   : game
    Created on : 30-Jan-2019, 22:06:55
    Author     : doobs
--%>

<%@page import="hu.evotraining.battleship.battleship.Battleship"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Battleship - GameBoard</title>
    </head>
    <body>
        <h1>BattleShip Board</h1>
        <table>
            <% 
                Battleship bs = (Battleship)session.getAttribute("battleship");
                for (int i = 0; i < bs.getGame().getGuessBoard().length; i++) {
                    %>
                    <tr>
                    <%
//                    System.out.println("<tr>");
                    for (int j = 0; j < bs.getGame().getGuessBoard()[0].length; j++) {
                        %>
                        <td><%= bs.getGame().getGuessBoard()[i][j] %></td><%
                    }
                    %></tr><%
                }
                //for log to output
                for (int i = 0; i < bs.getGame().getGuessBoard().length; i++) {
                        for (int j = 0; j < bs.getGame().getGuessBoard()[0].length; j++) {
                                System.out.println(bs.getGame().getGuessBoard()[i][j]);
                        }
                }
                System.out.println("");
            %>
        </table>
        <form action="GameServlet" method="get">
            X = <input type="text" name="x-guess" id="x-guess" ><br>
            Y = <input type="text" name="y-guess" id="y-guess" ><br>
            <input type="submit"  value="Shoot!">
        </form>
        <% 
            if(bs.getGame().isWin()) {
                %>
                <p>Nyertel!</p>
                <%
            } else {
                %>
                <p>No win so far!</p>
                <%
            }
        %>
    </body>
</html>
