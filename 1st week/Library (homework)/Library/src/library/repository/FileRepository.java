
package library.repository;

import java.io.FileNotFoundException;
import java.util.List;
import library.exception.InvalidFileInputException;
import library.model.Book;


public interface FileRepository {
    
    void fileLoad(String fileName) throws FileNotFoundException, InvalidFileInputException; //tárolt könyvek listája (txt)
    void updateStoraged() throws FileNotFoundException;; //tárolt könyvekbe írás listája (txt)
    void updateRented() throws FileNotFoundException;; //kivett könyvekbe írás (txt)
    
    
}
