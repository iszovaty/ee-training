<%-- 
    Document   : win
    Created on : 31-Jan-2019, 01:13:23
    Author     : doobs
--%>

<%@page import="hu.evotraining.battleship.battleship.Battleship"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <% 
            Battleship bs = (Battleship)session.getAttribute("battleship");
            //sessionokkel lehet szebb lett volna  
           if(bs.getGame().isWin()){
                   %>
                   <p>Congrat! You won!</p><%
            }
        %>
        
        <a href="WinServlet">Restart Game</a>
    </body>
</html>
