
package library.repository;

import java.util.ArrayList;
import java.util.List;
import library.model.Author;
import library.model.Book;

public interface BookRepository extends BaseRepository<Book>{
    
    List<Book> findAllTakens();
    List<Book> findByAuthor(Author author);
    Book findByAuthorAndTitle(Author author, String title);
    Book findRentedByAuthorAndTitle(Author author, String title);
    Book findByTitleWithoutAuthor(String title);
    void rentBook(Book book);
    void retakeBook(Book book);
    void save(Book book);
    void remove(Book book);
    
}
