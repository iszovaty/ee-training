/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.model;

/**
 *
 * @author doobs
 */
public class EducationalBook extends Book {

    public EducationalBook(Integer id, Author author, String title, String isbn, Integer numbOfPages) {
        super(id, author, title, isbn, numbOfPages);
    }

    public EducationalBook(Integer id, String title, String isbn, Integer numbOfPages) {
        super(id, title, isbn, numbOfPages);
    }

    @Override
    public String toString() {
        String out = "ID: " + id + " Author: ";
        out += (author != null) ? author.getName() : ""; 
        out += " Title: " + title + " ISBN: " + isbn + " Category: " + BookCategory.EDUCATIONAL.toString();
        return out;
    }
    
    
    
}
