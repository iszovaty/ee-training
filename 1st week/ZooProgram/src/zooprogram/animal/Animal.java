/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zooprogram.animal;

import java.util.Calendar;
import java.util.Date;
import zooprogram.exception.NegativeAgeException;

/**
 *
 * @author szotyi
 */

public abstract class Animal implements Comparable<Animal> {
    
    protected AnimalColor color;
    protected Date birthDate;
    
    
    public Animal(AnimalColor color, Date birthDate){
        this.color = color;
        this.birthDate = birthDate;
    }

    public AnimalColor getColor() {
        return color;
    }

    public void setColor(AnimalColor color) {
        this.color = color;
    }

    public int getAge() throws NegativeAgeException {
        Calendar birthCalendar = Calendar.getInstance();
        birthCalendar.setTime(birthDate);
        Date now = new Date();
        Calendar nowCalendar = Calendar.getInstance();
        nowCalendar.setTime(now);
        int age = nowCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR);
        if(age < 0){
            throw new NegativeAgeException("Jövőben született");
        }
        return age;
    }

    public void setAge(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " Animal{" + "color=" + color + ", birthDate=" + birthDate + '}';
    }

    @Override
    public int compareTo(Animal o) {
        if(getClass().getCanonicalName().compareTo(o.getClass().getCanonicalName()) == 0){
            return getColor().compareTo(o.getColor());
        }
        return getClass().getCanonicalName().compareTo(o.getClass().getCanonicalName());
    }
    
    
    
    
}
