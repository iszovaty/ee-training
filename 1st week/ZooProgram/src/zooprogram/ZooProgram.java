
package zooprogram;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import zooprogram.animal.Amur;
import zooprogram.animal.Animal;
import zooprogram.animal.AnimalColor;
import zooprogram.animal.AnimalFactory;
import zooprogram.animal.Bat;
import zooprogram.animal.Cat;
import zooprogram.animal.Chicken;
import zooprogram.animal.Constants;
import zooprogram.animal.Fly;
import zooprogram.animal.Raven;
import zooprogram.exception.NegativeAgeException;

/**
 *
 * @author szotyi
 */
public class ZooProgram {

    public static Date createDate(int year, int month, int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        return c.getTime();
    }

    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<>();
//        Random r = new Random();
//        
//        animals.add(new Cat(AnimalColor.BLACK, createDate(r.nextInt(2019) + 1999, r.nextInt(13) + 1, r.nextInt(31) + 1)));
//        animals.add(new Amur(AnimalColor.WHITE, createDate(r.nextInt(2019) + 1999, r.nextInt(13) + 1, r.nextInt(31) + 1), 75));
//        animals.add(new Bat(AnimalColor.BLACK, createDate(r.nextInt(2019) + 1999, r.nextInt(13) + 1, r.nextInt(31) + 1)));
//        animals.add(new Chicken(AnimalColor.ELSE, createDate(r.nextInt(2019) + 1999, r.nextInt(13) + 1, r.nextInt(31) + 1)));
//        animals.add(new Raven(AnimalColor.BLACK, createDate(r.nextInt(2019) + 1999, r.nextInt(13) + 1, r.nextInt(31) + 1)));
//        int db = 0;
//        for (int i = 0; i < animals.size(); i++) {
//            if(animals.get(i).getColor().equals(AnimalColor.BLACK) && animals.get(i) instanceof Fly ){
//                db++;
//            }
//            
//        }
//        System.out.println("the number of black flying animals: " + db);

        for (int i = 0; i < 5; i++) {
            animals.add(AnimalFactory.getAnimal(Constants.CAT));
            animals.add(AnimalFactory.getAnimal(Constants.BAT));
            animals.add(AnimalFactory.getAnimal(Constants.CHICKEN));
            animals.add(AnimalFactory.getAnimal(Constants.AMUR));
            animals.add(AnimalFactory.getAnimal(Constants.RAVEN));
        }
        for (int i = 0; i < animals.size(); i++) {
            System.out.println(animals.get(i).toString());
        }
        System.out.println("-----------SORTING ANIMALS------------");
        Collections.sort(animals);
        for (int i = 0; i < animals.size(); i++) {
            System.out.println(animals.get(i).toString());
        }
    }
}
