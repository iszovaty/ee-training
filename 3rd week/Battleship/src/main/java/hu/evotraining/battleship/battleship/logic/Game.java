
package hu.evotraining.battleship.battleship.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


public class Game {

    private String[][] gameBoard = new String[7][7];
    private String[][] guessBoard = new String[7][7];
    
      
    private void mapInit() throws FileNotFoundException{
        Scanner scanner = new Scanner(new File("battleship.map"));
        int i = 0;
        while(scanner.hasNext()){
            String[] line = scanner.nextLine().split(" ");
            int j = 0;
            while(j < line.length){
                gameBoard[i][j] = line[j];
                guessBoard[i][j] = "?";
                j++;
            }
            i++;
        }
        scanner.close();
    }
    
    public Game() throws FileNotFoundException {
        mapInit();
    }

    public String[][] getGameBoard() {
        return gameBoard;
    }

    public void setGameBoard(String[][] gameBoard) {
        this.gameBoard = gameBoard;
    }

    public String[][] getGuessBoard() {
        return guessBoard;
    }

    public void setGuessBoard(String[][] guessBoard) {
        this.guessBoard = guessBoard;
    }
    
    private void fileUpdate() throws FileNotFoundException{
        PrintWriter pw = new PrintWriter("battleship_result.map");
        for (int i = 0; i < guessBoard.length; i++) {
            for (int j = 0; j < guessBoard[0].length; j++) {
                pw.print(guessBoard[i][j]);
                if(j < guessBoard[0].length - 1){
                    pw.print(" ");
                }
            }
            pw.print("\n");
        }
        pw.close();
    }
    
    private void randomizeMap(){
        //under barkács processes
        System.out.println("not implemented so far :(");
    }
    
    public void guess(int x, int y) throws FileNotFoundException{
        if(x >= 0 && x < 8 && y >= 0 && y < 8){
            if(gameBoard[x][y].equals("1")){
                if(guessBoard[x][y].equals("?")){
                    gameBoard[x][y] = "-1";
                    guessBoard[x][y] = "-1";
                    //fileUpdate();
                } else{
                    //step is already done before
                }       
            } else if(gameBoard[x][y].equals("0")){
                if(guessBoard[x][y].equals("?")){
                    guessBoard[x][y] = "X";
                    //fileUpdate();
                } else {
                    //step is already done before
                }
            } else {
                //
            }
        }
        fileUpdate();
    }
    
    public boolean isWin(){
        int i = 0;
        int db = 0;
        while(i < gameBoard.length){
            int j = 0;
            while(j <gameBoard[0].length){
                if(gameBoard[i][j].equals("-1")){
                    db++;
                }
                j++;
            }
            i++;
        }
        if(db == 9){
            return true;
        }
        return false;
    }
    
}
