
package race;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import race.exceptions.DrawException;
import race.player.Player;
import race.player.PlayerContainer;


public class Race {

    
    private static Integer stringToNumeric(String literal){
        Integer val = -1;
        try {
          val = Integer.parseInt(literal);
        } catch(NumberFormatException nfe){
            val = -1;
            return val;
        }
        return val;
    }
    
    
    private static void fileLoad(String fileName, PlayerContainer playerContainer) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(fileName));
        while(scanner.hasNext()){
            List<Integer> results = new ArrayList<>();
            String[] line = scanner.nextLine().split(" ");
            if(line.length == 5){
                String name = line[0];
                results.add(stringToNumeric(line[1]));
                results.add(stringToNumeric(line[2]));
                results.add(stringToNumeric(line[3]));
                results.add(stringToNumeric(line[4]));
                playerContainer.getPlayers().add(new Player(name, results));
            }       
        }
        scanner.close();
    }
    

    public static void main(String[] args) {
        PlayerContainer playerContainer = new PlayerContainer();
        try {
            fileLoad("be.txt", playerContainer);
            System.out.println(playerContainer.getBestPlayer().toString());
        } catch(FileNotFoundException fnfe){
            System.out.println(fnfe.getMessage());
        } catch(DrawException de){
            System.out.println(de.getMessage());
        } finally {
            //..
        }
        
    }
    
}
