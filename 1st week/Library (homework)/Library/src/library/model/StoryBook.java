/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.model;

/**
 *
 * @author doobs
 */
public class StoryBook extends Book {

    public StoryBook(Integer id, Author author, String title, String isbn, Integer numbOfPages) {
        super(id, author, title, isbn, numbOfPages);
    }

    public StoryBook(Integer id, String title, String isbn, Integer numbOfPages) {
        super(id, title, isbn, numbOfPages);
    }
   
    @Override
    public String toString() {
        String out = "ID: " + id + " Author: ";
        out += (author != null) ? author.getName() : ""; 
        out += " Title: " + title + " ISBN: " + isbn + " Category: " + BookCategory.STORYBOOK.toString();
        return out;
    }
    
}
