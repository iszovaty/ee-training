/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bevezetofeladatok;

/**
 *
 * @author doobs
 */
public class FeladatVegzo {
    
    
    public static double atlag(Integer[] t){
        double avg = 0;
        int sum = 0;
        for (int i = 0; i < t.length; i++) {
            sum += t[i];
        }
        return ((double)sum) / ((double)(t.length));
    }   
    
    public static String mondatKepez(String[] szavak){
        String mondat = "";
        for (int i = 0; i < szavak.length; i++) {
            if(i % 2 != 0){
                mondat += szavak[i];
            }
            if(i < szavak.length - 2){
                mondat += " ";
            }
        }
        return mondat;
    }
    
    public static String mondatKepez2(String[] szavak){
        String mondat = "";
        for (int i = 1; i < szavak.length; i += 2) {
            mondat = mondat.concat(szavak[i]);
            if(i < szavak.length - 2){
                mondat = mondat.concat(" ");
            }
        }
        return mondat;
    }
    
    public static Boolean tombEgyezes(String[] t1, String[] t2){
        if(t1.length != t2.length){
            return false;
        } else {
            boolean egyenlo = true;
            for (int i = 0; i < t1.length && egyenlo; i++) {
                if(!t1[i].equals(t2[i])){
                    egyenlo = false;
                }
            }
            return egyenlo;
        }
    }
    
    public static Integer masodikMaxSzamIndex(Integer[] t){
        //int max1, max2 = -1; -> ez nem jó inicializálás? :O
        int max1 = -1;
        int max2 = -1;
        int max1index = -1;
        int max2index = -1;
        for (int i = 0; i < t.length; i++) {
            if(t[i] > max1){
                max2 = max1;
                max2index = max1index;
                max1 = t[i];
                max1index = i;
            } else if(t[i] == max1){
                max1index = i;
            } else if(t[i] == max2){
                max2index = i;
            } 
        }
        return max2index;
    }
    

    
    public static Integer[][] nagyobbakKivalogat(Integer[][] mtx1, Integer[][] mtx2){
        if(mtx1.length != mtx2.length || mtx1[0].length != mtx2[0].length){
            return null;
        }
        Integer kimenet[][] = new Integer[mtx1.length][mtx1[0].length];
        for (int i = 0; i < mtx1.length; i++) {
            for (int j = 0; j < mtx1[0].length; j++) {
                if(mtx1[i][j] >= mtx2[i][j]){
                    kimenet[i][j] = mtx1[i][j];
                } else {
                    kimenet[i][j] = mtx2[i][j];
                }
            }
        }
        return kimenet;
    } 
    
        public static String matrixListaz(Integer[][] mtx){
        String kimenet = "";
        for (int i = 0; i < mtx.length; i++) {
            for (int j = 0; j < mtx[0].length; j++) {
                kimenet += mtx[i][j] + " ";
            }
            kimenet += "\n";
        }
        return kimenet;
    }
    
    
}
