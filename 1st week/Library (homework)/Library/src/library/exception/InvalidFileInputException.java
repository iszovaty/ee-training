
package library.exception;

public class InvalidFileInputException extends Exception {

    public InvalidFileInputException(String message) {
        super(message);
    }  
    
    
}
