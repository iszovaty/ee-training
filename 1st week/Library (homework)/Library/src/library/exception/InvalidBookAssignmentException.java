
package library.exception;

public class InvalidBookAssignmentException extends Exception {

    public InvalidBookAssignmentException(String message) {
        super(message);
    }
    
    
}
