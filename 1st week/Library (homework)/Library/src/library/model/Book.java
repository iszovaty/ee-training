
package library.model;


public abstract class Book implements Comparable<Book> {
 
    protected Integer id;
    protected Author author;
    protected String title;
    protected String isbn;
    protected Integer numbOfPages;

    private Book(){
        //
    }
    
    public Book(Integer id, Author author, String title, String isbn, Integer numbOfPages) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.isbn = isbn;
        this.numbOfPages = numbOfPages;
    }

    public Book(Integer id, String title, String isbn, Integer numbOfPages) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.numbOfPages = numbOfPages;
    } 

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Integer getNumbOfPages() {
        return numbOfPages;
    }

    public void setNumbOfPages(Integer numbOfPages) {
        this.numbOfPages = numbOfPages;
    }

    
    
    @Override
    public int compareTo(Book o){
//        if(getClass().getSimpleName().compareTo((o.getClass().getSimpleName())) == 0){
//            return getTitle().compareTo(o.getTitle());
//        }
//        return getClass().getSimpleName().compareTo(o.getClass().getSimpleName());
        return getId().compareTo(o.getId());
    }

 
    @Override
    public abstract String toString();
    
}
