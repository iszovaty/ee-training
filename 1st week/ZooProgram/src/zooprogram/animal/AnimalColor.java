/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zooprogram.animal;

/**
 *
 * @author szotyi
 */
public enum AnimalColor {
    
    BLACK(0),
    WHITE(1),
    ELSE(2);
    
    private int color;
    
    AnimalColor(int color){
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
    
    
    

}
