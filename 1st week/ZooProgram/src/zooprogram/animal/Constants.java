/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zooprogram.animal;

/**
 *
 * @author szotyi
 */
public interface Constants {
    
    public static final String CAT = "Cat";
    public static final String BAT = "Bat";
    public static final String RAVEN = "Raven";
    public static final String AMUR = "Amur";
    public static final String CHICKEN = "Chicken";
}
