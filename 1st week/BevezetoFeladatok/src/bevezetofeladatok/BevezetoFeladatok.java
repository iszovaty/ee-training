
package bevezetofeladatok;

public class BevezetoFeladatok {

    //1.feladat
    public static void elsoFeladat(){
        Integer[] tomb = new Integer[]{10,12,16,7,2}; 
        System.out.println(FeladatVegzo.atlag(tomb));
    }
    
    //2.feladat
    public static void masodikFeladat(){
        String[] tomb2 = new String[]{"nem","hello","nem","world"};
        System.out.println(FeladatVegzo.mondatKepez2(tomb2));
    }
    
    //3.feladat
    public static void harmadikFeladat(){
        String[] tomb2 = new String[]{"C", "A", "F", "D", "E"};
        String[] tomb3 = new String[]{"A","B","C","D","E"};
        String[] tomb4 = new String[]{"A","B","C","D","E"};
        System.out.println(FeladatVegzo.tombEgyezes(tomb2, tomb3));
        System.out.println(FeladatVegzo.tombEgyezes(tomb3, tomb4));
    }
    
    //4.feladat
    public static void negyedikFeladat(){
        Integer[] tomb5 = new Integer[]{1,2,2,3,2,5};
        int index = FeladatVegzo.masodikMaxSzamIndex(tomb5);
        if(index != -1){
            System.out.println(index);
        } else {
            System.out.println("Nincs masodik legnagyobb");
        }
    }
    
    public static void otodikFeladat(){
        Integer[][] mtx1 = new Integer[][]{
            {1, 2, 3, 4},
            {2, 5, 6, 7},
            {3, 4, 6, 2}
        };
        Integer[][] mtx2 = new Integer[][]{
            {2, 2, 3, 1},
            {3, 5, 3, 8},
            {3, 4, 6, 9}
        };
        Integer[][] kimenet = FeladatVegzo.nagyobbakKivalogat(mtx1, mtx2);
        if(kimenet == null){
            System.out.println("A 2 mátrix nem azonos méretű");
        } else {
            System.out.println(FeladatVegzo.matrixListaz(kimenet));
        }
    }
    
    public static void feladatVegzo(){
        elsoFeladat();
        masodikFeladat();
        harmadikFeladat();
        negyedikFeladat();
        otodikFeladat();
    }
    
    public static void main(String[] args) {  
        feladatVegzo();
        
    }
    
}
