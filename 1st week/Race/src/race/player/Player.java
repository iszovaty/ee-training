/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package race.player;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author doobs
 */
public class Player {
    
    private String name;
    private List<Integer> results = new ArrayList<>();
    private Boolean validJump = false;
    
    private Player(){
        
    }
    
    public Player playerFactory(String name, List<Integer> results){
        
        Player player = new Player();
        player.setName(name);
        List<Integer> jumps = new ArrayList<>();
        for (int i = 0; i < results.size(); i++) {
            if(results.get(i) != -1){
                setValidJump(true);
            }
            jumps.add(results.get(i));
        }
        player.setResults(jumps);
        return player;
    }
    
    public Player(String name, List<Integer> results) {
        this.name = name;
        for (int i = 0; i < results.size(); i++) {
            if(results.get(i) != -1){
                setValidJump(true);
            }
            this.results.add(results.get(i));
        }
    }

    public List<Integer> getResults() {
        return results;
    }

    public void setResults(List<Integer> results) {
        this.results = results;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getValidJump() {
        return validJump;
    }

    public void setValidJump(Boolean validJump) {
        this.validJump = validJump;
    }


    
    public Integer[] bestResult(){
        Integer[] best = new Integer[]{-1, -1};
        if(getValidJump()){
            for(int i = 0; i < results.size(); i++){
                if(results.get(i) > best[0]){
                    best[0] = results.get(i);
                    best[1] = i;
                }
            }
        } 
        return best;
    }

    @Override
    public String toString() {
        return "The winner is " + name + " with " + bestResult()[0];
    }
    
    
}
