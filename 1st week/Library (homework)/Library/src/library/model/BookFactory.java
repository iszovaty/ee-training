
package library.model;

import java.util.Random;


public class BookFactory implements Constants {
    
    private static int generatedId = 1;
    
    public static Book createBook(Author author, String title, String isbn,Integer numbOfPages, BookCategory category) {
        Integer id = generatedId++;
        //ez egyenlőre valószínüleg fölösleges, mert elvielg ha gond van akkor már kivül dobtunk rá exception-t
        //ha meg nincs gond akkor meg beugrik a switch ágba (tehát nem null-al tér vissza)
        if(!title.isEmpty() && !isbn.isEmpty() && numbOfPages > 0 && category != null){
            switch(category){
                case SCIFI:
                    return new ScifiBook(id, author, title, isbn, numbOfPages);
                case COOKBOOK: 
                    return new CookBook(id, author, title, isbn, numbOfPages);
                case LITERATURE:
                    return new LiteratureBook(id, author, title, isbn, numbOfPages);
                case EDUCATIONAL:
                    return new EducationalBook(id, author, title, isbn, numbOfPages);
                case STORYBOOK: 
                    return new StoryBook(id, author, title, isbn, numbOfPages);
            }
        }
        return null;
        
    }
      
}
