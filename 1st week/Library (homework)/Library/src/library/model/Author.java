
package library.model;

import java.util.ArrayList;
import java.util.List;


public class Author {
    
    private Integer id;
    private String name;
    private List<Book> writtenBooks = new ArrayList<>();

    private Author(){
        
    }
    
    public Author(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getWrittenBooks() {
        return writtenBooks;
    }

    public void setWrittenBooks(List<Book> writtenBooks) {
        this.writtenBooks = writtenBooks;
    }

    @Override
    public String toString() {
        return "ID: " + id + " Name: " + name;
    }
    
   public static Author getInstance(){
       return new Author();
   }
    
    
    
    
}
