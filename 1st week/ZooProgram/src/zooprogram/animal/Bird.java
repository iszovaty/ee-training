/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zooprogram.animal;

import java.util.Date;

/**
 *
 * @author szotyi
 */
public abstract class Bird extends Animal {
    
    public Bird(AnimalColor color, Date birthDate) {
        super(color, birthDate);
    }
    
}
